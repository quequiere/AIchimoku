﻿using AIChimoku.Models;
using AIChimoku.Services;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;
using AIChimoku.Services.AIServices;
using System.Threading.Tasks;

namespace AIChimoku
{
    public class MainViewModel
    {
        public static PlotModel displayedChart { get; set; }
        public PlotModel Chart { get; private set; }

        private ChartService chartService { get; }
        private AIService aIService { get; }

        public MainViewModel(ChartService chartService, AIService aIService)
        {
            this.chartService = chartService;
            this.aIService = aIService;

            this.initGraph();
            this.initCandles();
            this.initAI();
        }

        private void initAI()
        {
            var ichimoku = this.chartService.getIchimoku();
            Task.Run(async delegate
            {
                await Task.Delay(1500);
                aIService.StartAnalysis(ichimoku);

            });
           
        }

        private void initGraph()
        {
            Chart = new PlotModel { Title = "AIChimoku chart" };
            displayedChart = Chart;


            var timeSpanAxis1 = new DateTimeAxis {
                Position = AxisPosition.Bottom, 
                StringFormat = "MM/yyyy" ,
                IntervalType = DateTimeIntervalType.Months
            };

            var linearAxis1 = new LinearAxis
            {
                Position = AxisPosition.Left
            };


            Chart.Axes.Add(timeSpanAxis1);
            Chart.Axes.Add(linearAxis1);
        }

        private void initCandles()
        {

            var ichimoku = this.chartService.getIchimoku();

            this.Chart.Series.Add(ichimoku.cloud);
            this.Chart.Series.Add(ichimoku.candles);
            this.Chart.Series.Add(ichimoku.tenkan);
            this.Chart.Series.Add(ichimoku.kinjun);
            this.Chart.Series.Add(ichimoku.lagging);

        }

      

    }
}
