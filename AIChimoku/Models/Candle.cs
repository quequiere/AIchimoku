﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIChimoku.Models
{
    public class Candle
    {
        public DateTime Time { get; set; }
        public double H { get; set; }
        public double L { get; set; }
        public double O { get; set; }
        public double C { get; set; }
    }
}
