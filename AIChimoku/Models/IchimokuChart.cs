﻿using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using OxyPlot;
using OxyPlot.Axes;

namespace AIChimoku.Models
{
    public class IchimokuChart
    {
        public AreaSeries cloud { get; }
        public CandleStickSeries candles { get; }
        public LineSeries tenkan { get; }
        public LineSeries kinjun { get; }
        public LineSeries lagging { get; }

        public IchimokuChart(AreaSeries cloud, CandleStickSeries candles,  LineSeries tenkan, LineSeries kinjun, LineSeries lagging)
        {
            this.cloud = cloud;
            this.candles = candles;
            this.tenkan = tenkan;
            this.kinjun = kinjun;
            this.lagging = lagging;
        }

        private List<Candle> getCandlesPrices()
        {
            return candles.ItemsSource.Cast<Candle>().ToList();

        }

        //Transform all ichimoku chart to sub units and ignore thoses are not complete
        public IEnumerable<IchimokuUnit> toUnits()
        {
            //foreach (var c in this.getCandlesPrices())
            //{
            //    var axeX = DateTimeAxis.ToDouble(c.Time);

            //    var u =  new IchimokuUnit {
            //        time = c.Time,
            //        candleAxeX = axeX,
            //        SSA = this.cloud.Points.Where(p => p.X == axeX).FirstOrDefault().Y,
            //        SSB = this.cloud.Points2.Where(p => p.X == axeX).FirstOrDefault().Y,
            //        tenkan = this.tenkan.ItemsSource.Cast<DataPoint>().Where(p => p.X == axeX).FirstOrDefault().Y,
            //        kinjun = this.kinjun.ItemsSource.Cast<DataPoint>().Where(p => p.X == axeX).FirstOrDefault().Y,
            //        lagging = this.lagging.ItemsSource.Cast<DataPoint>().Where(p => p.X == axeX).FirstOrDefault().Y,
            //        priceClose= c.C
            //    };

            //    if(u.SSA != 0 && u.SSB != 0 && u.tenkan != 0 && u.kinjun !=0 && u.lagging !=0)
            //        yield return u;

            //}

            //On se base sur le cloud car il apparait le plus tard et reste le plus longtemps sur le chart
            foreach (var c in this.cloud.Points)
            {
                var axeX = c.X;
                var time = DateTimeAxis.ToDateTime(axeX);


                var price = this.getCandlesPrices().Where(p => p.Time == time).FirstOrDefault();


                var u = new IchimokuUnit
                {
                    time = time,
                    candleAxeX = axeX,
                    SSA = c.Y,
                    SSB = this.cloud.Points2.Where(p => p.X == axeX).FirstOrDefault().Y,
                    tenkan = this.tenkan.ItemsSource.Cast<DataPoint>().Where(p => p.X == axeX).FirstOrDefault().Y,
                    kinjun = this.kinjun.ItemsSource.Cast<DataPoint>().Where(p => p.X == axeX).FirstOrDefault().Y,
                    lagging = this.lagging.ItemsSource.Cast<DataPoint>().Where(p => p.X == axeX).FirstOrDefault().Y,
                    priceClose = (price == null) ? 0 : price.C
                };

                    yield return u;

            }
        }
    }
}
