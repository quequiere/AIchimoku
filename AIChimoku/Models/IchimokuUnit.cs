﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIChimoku.Models
{
    public class IchimokuUnit
    {
        public DateTime time { get; set; }
        public double candleAxeX {get; set; }
        public double SSA { get; set; }
        public double SSB { get; set; }
        public double tenkan { get; set; }
        public double kinjun { get; set; }
        public double lagging { get; set; }
        public double priceClose { get; set; }
    }
}
