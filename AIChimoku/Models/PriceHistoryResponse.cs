﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIChimoku.Models
{
    public class PriceHistoryResponse
    {
        public bool status { get; set; }
        public string msg { get; set; }
        public List<ResponseProviderCandle> response { get; set; }
    }

    public class ResponseProviderCandle
    {
        public double o { get; set; }
        public double h { get; set; }
        public double l { get; set; }
        public double c { get; set; }
        public double t { get; set; }
        public DateTime tm { get; set; }
    }
}
