﻿using ArtificialNeuralNetwork;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using AIChimoku.Services.AIServices;

namespace AIChimoku.Models
{
    public class ScorableNetwork
    {
        public INeuralNetwork network;
        public double finalScore = -1;

        public Dictionary<DateTime,double> takenPos;

        public ScorableNetwork()
        {
            this.network = AITools.CreateRandomNetwork();
        }

        public ScorableNetwork(INeuralNetwork network)
        {
            this.network = network.CopyNetwork();
        }

        public int CompareTo([AllowNull] ScorableNetwork other)
        {
            return this.finalScore.CompareTo(other.finalScore);
        }

        public double[] compute(double[] inputs)
        {
            return this.network.ComputeNetwork(inputs);
        }
    }
}
