﻿using AIChimoku.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AIChimoku.Providers
{
    public class FcsapiFileProvider : IPriceProvider
    {

        public PriceHistoryResponse GetDailyCandles()
        {
            var currentDirectory  = Directory.GetCurrentDirectory();
            var text = File.ReadAllText($"{currentDirectory}/Files/candles.json", Encoding.UTF8);
            var response = JsonConvert.DeserializeObject<PriceHistoryResponse>(text);
            return response;
        }
     
    }
}
