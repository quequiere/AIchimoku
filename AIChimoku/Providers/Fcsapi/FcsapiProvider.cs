﻿using AIChimoku.Models;
using Microsoft.Extensions.Options;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace AIChimoku.Providers
{
    public class FcsapiProvider : IPriceProvider
    {
        private RestClient client { get; } = new RestClient("https://fcsapi.com/api/");


        private readonly IOptions<ConfigFcsapi> configFcsapi;


        public FcsapiProvider(IOptions<ConfigFcsapi> configFcsapi)
        {
            this.configFcsapi = configFcsapi;
        }


        public PriceHistoryResponse GetDailyCandles()
        {
            var request = new RestRequest("forex/history", Method.GET);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            request.AddParameter("symbol", "EUR/USD");
            request.AddParameter("period", "1d");
            request.AddParameter("access_key", configFcsapi.Value.apikey);

            var result = client.Execute<PriceHistoryResponse>(request);

            return result.Data;

        }
     
    }
}
