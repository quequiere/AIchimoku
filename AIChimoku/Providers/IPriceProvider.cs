﻿using AIChimoku.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AIChimoku.Providers
{
    public interface IPriceProvider
    {
         PriceHistoryResponse GetDailyCandles();
    }
}
