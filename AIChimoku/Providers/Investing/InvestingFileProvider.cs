﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AIChimoku.Models;

namespace AIChimoku.Providers.Investing
{
    public class InvestingFileProvider : IPriceProvider
    {
        public PriceHistoryResponse GetDailyCandles()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var text = File.ReadAllText($"{currentDirectory}/Files/eurusd-InvestingCom.csv", Encoding.UTF8);

            var candles = text.Split("\n").Skip(1).Select(row => {

                var data = row.Split("\",\"")
                .Select(d => d.Replace("\"", "").Trim())
                .Select(d => d.Replace(",","."))
                .ToList();

                var timeUnit = data[0].Split("/")
                .Select(u => int.Parse(u))
                .ToList();

                var dateTime = new DateTime(timeUnit[2], timeUnit[1], timeUnit[0]);
                var close = double.Parse(data[1]);
                var open = double.Parse(data[2]);
                var high = double.Parse(data[3]);
                var low = double.Parse(data[4]);

                return new ResponseProviderCandle()
                {
                    c = close,
                    o = open,
                    h = high,
                    l = low,
                    tm = dateTime
                };
            }).OrderBy(c => c.tm).ToList();


            return new PriceHistoryResponse()
            {
                msg = "CSV ouput",
                status = true,
                response = candles
            };
        }
    }
}
