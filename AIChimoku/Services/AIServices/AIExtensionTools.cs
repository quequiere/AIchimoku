﻿using ArtificialNeuralNetwork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AIChimoku.Services.AIServices
{
    public static class AIExtensionTools
    {
        public static List<INeuron> GetAllNeurons(this INeuralNetwork network)
        {
            var list = new List<INeuron>();

            //This one has two layer, one is connected to input L0, the other one to L1
            list.AddRange(network.HiddenLayers.SelectMany(layer => layer.NeuronsInLayer).ToList());
            list.AddRange(network.OutputLayer.NeuronsInLayer.ToList());
            return list;
        }

        public static void ApplyCrossOver(this INeuralNetwork network, INeuralNetwork partner, double chance)
        {

            var otherNeurones = partner.GetAllNeurons();

            network.GetAllNeurons()
                .Select((neuron, index) => (neuron, index))
                .ToList()
                .ForEach(n => {
                    n.neuron.Soma.Dendrites.Select((dendrite, identrite) => (dendrite, identrite)).ToList().ForEach(s =>
                    {
                        s.dendrite.Weight = AITools.TryMixRandom(chance, s.dendrite.Weight, otherNeurones[n.index].Soma.Dendrites[s.identrite].Weight);
                    });
                });

        }

        public static void ApplyMutation(this INeuralNetwork network, double chance)
        {
            network.GetAllNeurons().ForEach(n => {
                n.Soma.Dendrites.ToList().ForEach(synapse =>
                {
                    synapse.Weight = AITools.TryChangeRandom(chance, synapse.Weight);
                });
            });


        }

        public static double[] ComputeNetwork(this INeuralNetwork network, double[] inputs)
        {
            network.SetInputs(inputs);
            network.Process();
            return network.GetOutputs();
        }

        public static INeuralNetwork CopyNetwork(this INeuralNetwork network)
        {
            return AITools.GetNeuralFactoryInstance().Create(network.GetGenes());
        }
    }
}
