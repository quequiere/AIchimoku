﻿using AIChimoku.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using AIChimoku.Services.AIServices;
using AIChimoku.PriceAnalyses.Services;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using System.Windows;

namespace AIChimoku.Services.AIServices
{
    public class AIService
    {
        private static int parentsInitializationSize  = 20;
        private static int childPerGeneration  = 2000;
        private static int generationNumber  = 9999999;
        private static double mutationChance = 0.05;
        private static int threadNumber = 20;
        private static int parentsToKeep = 10;


        private int computedGeneration = 0;
        private List<ScorableNetwork> bestParents = new List<ScorableNetwork>();
        private IchimokuChart ichimokuChart { get; set; }

        private List<AreaSeries> networkRenders = new List<AreaSeries>();

        public void StartAnalysis(IchimokuChart ichimoku)
        {
            this.ichimokuChart = ichimoku;

            var startParents = Enumerable.Range(0, parentsInitializationSize - 1).Select(_ => new ScorableNetwork()).ToList();
            bestParents.AddRange(startParents);
            Enumerable.Range(0, generationNumber - 1).ToList().ForEach(i => this.PlayGeneration());


        }


        private void PlayGeneration()
        {
            Debug.WriteLine($"Starting generation {computedGeneration++} with {bestParents.Count} parents.");

            // Generation Time
            var networks = GenerateGeneration(childPerGeneration);

            // Calculation Time
            this.RunAnalyses(networks);

            //Score Time


            var eliteGeneation = networks
                .OrderByDescending(n => n.finalScore)
                .Take(parentsToKeep)
                .ToList();

            bestParents.AddRange(eliteGeneation);

            bestParents = bestParents
                .OrderByDescending(n => n.finalScore)
                .Take(parentsToKeep)
                .ToList();


            Debug.WriteLine("Parents updated:");
            bestParents.ForEach(p => {
                Debug.WriteLine($"- {p.finalScore} pos taken: {p.takenPos.Count}");
            });


            generateGraph(bestParents.First());



        }

        // ============================== IA DISPLAY GRAPH ===============================================

        private OxyColor doubleToColor(double value)
        {
            //Careful with negativ !
            var h = OxyColors.Green;

            return OxyColor.FromArgb(Convert.ToByte(255* value), h.R, h.G, h.B);
        }

        private void generateGraph(ScorableNetwork network)
        {
            Application.Current.Dispatcher.Invoke(new Action(() => {


                networkRenders.ForEach(n => MainViewModel.displayedChart.Series.Remove(n));


                var max = network.takenPos.OrderByDescending(t => t.Value).First();

                networkRenders = network.takenPos.Where(dp => dp.Value>0).Select(dp => {

                                                var area = new AreaSeries()
                                                {
                                                    Fill = doubleToColor(dp.Value),
                                                    StrokeThickness = 1
                                                };

                                                area.Points.Add(new DataPoint(DateTimeAxis.ToDouble(dp.Key),1.25));
                                                area.Points2.Add(new DataPoint(DateTimeAxis.ToDouble(dp.Key),1.05));
                             
                                                area.Points.Add(new DataPoint(DateTimeAxis.ToDouble(dp.Key)+1,1.25));
                                                area.Points2.Add(new DataPoint(DateTimeAxis.ToDouble(dp.Key)+1,1.05));
                             

                                                return area;
                                            }).ToList();


                //var baseValue = network.takenPos.Select(d => new DataPoint(DateTimeAxis.ToDouble(d.Key), 1)).ToList();

                //networkRender.Points.Clear();
                //networkRender.Points2.Clear();

                //networkRender.Points.AddRange(scoreValue);
                //networkRender.Points2.AddRange(baseValue);

                networkRenders.ForEach(n => MainViewModel.displayedChart.Series.Add(n));

                //MainViewModel.displayedChart.Series.Add(networkRender);

                MainViewModel.displayedChart.InvalidatePlot(true);


            }));


         
        }



        // ============================== IA GENERATION ===============================================



        private List<ScorableNetwork> GenerateGeneration(int quantity)
        {
            var startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            var quantityByThread = quantity / bestParents.Count;

            var createdNetworks = bestParents
                .AsParallel()
                .Select(n => {
                    return CreateScorablePack(quantityByThread, n);
                })
                .SelectMany(n => n)
                .ToList();

            var networkCreationTime = (DateTimeOffset.Now.ToUnixTimeMilliseconds() - startTime) / 1000.0;
            Debug.WriteLine($"Created {createdNetworks.Count} networks in {networkCreationTime} seconds");


            return createdNetworks;

        }


        private IEnumerable<ScorableNetwork> CreateScorablePack(int quantity, ScorableNetwork parentNetwork)
        {
            Random r = new Random();

            for (int x = 0; x < Math.Ceiling(quantity / 2.0); x++)
            {
                var toMute = parentNetwork.network.CopyNetwork();
                toMute.ApplyMutation(mutationChance);

                yield return new ScorableNetwork(toMute);

                var randomPartner = bestParents[r.Next(0, bestParents.Count)];
                var toCross = parentNetwork.network.CopyNetwork();

                toCross.ApplyCrossOver(randomPartner.network, r.NextDouble());
                toCross.ApplyMutation(mutationChance);

                yield return new ScorableNetwork(toCross);
            }
        }

        // ======================================== Run evaluation Job ============

        private void RunAnalyses(List<ScorableNetwork> networks)
        {
            var startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            var networksByGames = networks
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / threadNumber)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();

            var ichimokuUnits = ichimokuChart.toUnits().ToList();

            networksByGames.AsParallel()
                .ForAll(netList => {
                    netList.ForEach(network => {
                        new PriceAnalyser(network).computeAnalyse(ichimokuUnits);
                    });
                });

            var endTime = (DateTimeOffset.Now.ToUnixTimeMilliseconds() - startTime) / 1000.0;
            Debug.WriteLine($"Finished computing in {endTime} seconds");
        }
    }
}
