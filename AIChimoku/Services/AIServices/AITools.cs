﻿using ArtificialNeuralNetwork;
using ArtificialNeuralNetwork.ActivationFunctions;
using ArtificialNeuralNetwork.Factories;
using ArtificialNeuralNetwork.WeightInitializer;
using System;
using System.Collections.Generic;
using System.Text;

namespace AIChimoku.Services.AIServices
{
    public class AITools
    {

        public static INeuralNetwork CreateRandomNetwork()
        {
            var numInputs = 97;
            var numOutputs = 1;
            var numHiddenLayers = 4;
            var numNeuronsInHiddenLayer = 20;
            return GetNeuralFactoryInstance().Create(numInputs, numOutputs, numHiddenLayers, numNeuronsInHiddenLayer);

        }

        public static NeuralNetworkFactory GetNeuralFactoryInstance()
        {
            var somaFactory = SomaFactory.GetInstance(new SimpleSummation());
            //var axonFactory = AxonFactory.GetInstance(new RectifiedLinearActivationFunction());
            var axonFactory = AxonFactory.GetInstance(new TanhActivationFunction());
            var randomInit = new RandomWeightInitializer(new Random());
            var hiddenSynapseFactory = SynapseFactory.GetInstance(randomInit, axonFactory);
            var ioSynapseFactory = SynapseFactory.GetInstance(new ConstantWeightInitializer(1.0), axonFactory);
            var neuroFactory = NeuronFactory.GetInstance();
            var biasInitializer = randomInit;
            return NeuralNetworkFactory.GetInstance(somaFactory, axonFactory, hiddenSynapseFactory, ioSynapseFactory, biasInitializer, neuroFactory);

        }

        public static double TryMixRandom(double chance, double firstValue, double secondValue)
        {
            Random rnd = new Random();
            double chanceChange = rnd.NextDouble();

            if (chanceChange <= chance)
            {
                return secondValue;
            }
            return firstValue;
        }

        public static double TryChangeRandom(double chance, double originalValue)
        {
            Random rnd = new Random();
            double chanceChange = rnd.NextDouble();

            if (chanceChange <= chance)
            {
                double value = rnd.NextDouble();
                if (rnd.NextDouble() < 0.5) value *= -1;
                return value;
            }
            return originalValue;
        }
    }
}
