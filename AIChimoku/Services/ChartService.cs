﻿using AIChimoku.Models;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;
using OxyPlot.Axes;

namespace AIChimoku.Services
{
    public class ChartService
    {
        private PriceService priceService { get; }
        private IchimokuService ichimokuService { get; }

        public ChartService(PriceService priceService, IchimokuService ichimokuService)
        {
            this.priceService = priceService;
            this.ichimokuService = ichimokuService;
        }

        public IchimokuChart getIchimoku()
        {
            var candlesPrices = this.priceService.getDayHistory();

            CandleStickSeries candles = new CandleStickSeries()
            {
                Color = OxyColors.Black,
                IncreasingColor = OxyColor.FromRgb(0, 197, 49),
                DecreasingColor = OxyColor.FromRgb(255, 95, 95),
                DataFieldX = "Time",
                DataFieldHigh = "H",
                DataFieldLow = "L",
                DataFieldClose = "C",
                DataFieldOpen = "O",
                TrackerFormatString = "Date: {2}\nOpen: {5:0.00000}\nHigh: {3:0.00000}\nLow: {4:0.00000}\nClose: {6:0.00000}",

            };

            candles.ItemsSource = candlesPrices;


            var tenkan = this.GenerateLineFromDouble(candlesPrices.Skip(9).Select(c => GetNextDay(c.Time, -1)).ToList(), this.ichimokuService.GetTenkan(candlesPrices), OxyColors.Brown);
            var kinjun = this.GenerateLineFromDouble(candlesPrices.Skip(26).Select(c => GetNextDay(c.Time, -1)).ToList(), this.ichimokuService.GetKinjun(candlesPrices), OxyColors.Blue);
            var lagging = this.GenerateLineFromDouble(candlesPrices.SkipLast(26).Select(c => c.Time).ToList(), this.ichimokuService.GetLaggingSpan(candlesPrices), OxyColors.Green);


            var ssa = this.ichimokuService.GetSSA(candlesPrices);
            var ssb = this.ichimokuService.GetSSB(candlesPrices);
            var cloudWorkset = candlesPrices.Skip(52).ToList();


            var ssTimes = cloudWorkset.Select(c =>
            {
                //Don't know why but need to add 9 more days
                var projection = c.Time.AddDays(26 + 9);
                var modificator = projection.DayOfWeek switch
                {
                    DayOfWeek.Sunday => 1,
                    DayOfWeek.Saturday => 2,
                    _ => 0

                };


                return projection.AddDays(modificator);
            }

            ).ToList();
            var cloud = GenerateArea(ssTimes, ssa, ssb);

            return new IchimokuChart(cloud, candles, tenkan, kinjun, lagging);
        }



        private DateTime GetNextDay(DateTime time, int operation)
        {
            var isPos = operation > 0 ? true : false;

            var newDate = time.AddDays(operation);

            var modificator = newDate.DayOfWeek switch
            {
                DayOfWeek.Sunday => isPos ? 1 : -2,
                DayOfWeek.Saturday => isPos ? 2 : -1,
                _ => 0
            };

            return newDate.AddDays(modificator);
        }

        private AreaSeries GenerateArea(List<DateTime> times, List<double> doubles, List<double> doubles2)
        {

            var points = DoubleToData(times, doubles);
            var points2 = DoubleToData(times, doubles2);

            var s = new AreaSeries()
            {
                Color2 = OxyColors.Red,
                StrokeThickness = 1,
            };

            s.Points.AddRange(points);
            s.Points2.AddRange(points2);

            return s;
        }

        private LineSeries GenerateLineFromDouble(List<DateTime> times, List<double> doubles, OxyColor color)
        {

            var points = DoubleToData(times, doubles);

            return new LineSeries
            {
                StrokeThickness = 1,
                Color = color,
                ItemsSource = points,
                Selectable = false
            };
        }

        private List<DataPoint> DoubleToData(List<DateTime> times, List<double> doubles)
        {
            return times.Select((c, i) => (c, i))
             .Select((cur) => new DataPoint(DateTimeAxis.ToDouble(cur.c), doubles[cur.i]))
             .ToList();
        }

    }
}
