﻿using AIChimoku.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AIChimoku.Services
{
    public class IchimokuService
    {
        /// <summary>
        /// Plus haut et plus bas sur les dernières 26 périodes. Marque supports resistances
        /// </summary>
        /// <param name="candles"></param>
        public List<double> GetKinjun(List<Candle> candles)
        {
            return GetTopMinMoy(candles, 26).ToList();
        }

        /// <summary>
        /// Plus haut et plus bas sur les dernières 9 périodes. Marque équilibre des prix
        /// </summary>
        /// <param name="candles"></param>
        public List<double> GetTenkan(List<Candle> candles)
        {
            return GetTopMinMoy(candles, 9).ToList();
        }

        public List<double> GetLaggingSpan(List<Candle> candles)
        {
            return candles.Select(c => c.C).Skip(26).ToList();
        }


        public List<double> GetSSA(List<Candle> candles)
        {
            var kinjung = this.GetKinjun(candles);
            //On skip car la tenkan commence a la 9eme periode et kinjun a 26. On doit les ramener au meme moment.
            var tenkan = this.GetTenkan(candles).Skip(17).ToList();

            if (kinjung.Count() != tenkan.Count())
                throw new Exception("Error to synchronyse prices");


            //On skip pour avoir la meme synchro time que la SSB qui démarre à 52
            return kinjung.Select((k, i) => (k + tenkan[i]) / 2.0).Skip(26).ToList();
        }

        public List<double> GetSSB(List<Candle> candles)
        {
            return GetTopMinMoy(candles, 52).ToList();
        }


        private IEnumerable<double> GetTopMinMoy(List<Candle> candles, int period)
        {
            var candleToAnalyse = candles.Skip(period).Select((cand, i) => (cand, i)).OrderBy(c => c.cand.Time).ToList();

            foreach (var c in candleToAnalyse)
            {
                var max = candles.Skip(c.i).Take(period).Max(c => c.H);
                var min = candles.Skip(c.i).Take(period).Min(c => c.L);

                yield return (min + max) / 2.0; ;
            }
        }
    }
}
