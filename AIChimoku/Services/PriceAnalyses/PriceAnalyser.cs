﻿using AIChimoku.Models;
using AIChimoku.Services.PriceAnalyses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;

namespace AIChimoku.PriceAnalyses.Services
{
    /// <summary>
    /// Le price Analyser prend utilise le réseau neuronale pour lui donner une taille de la position à prendre de 0 à 1.
    ///Si la position à prendre est inférieure à minPosition le réseau ne prend pas la position.
    ///On regarde par la suite après evaluationNextPos unité de temps si nous avons fait des bénéfices en fonction de la position prise
    ///Ces derniers, positifs ou négatifs, sont ajoutés au score. Il croissent de manière exponentielle pour encourager l'IA aux positions longues.
    /// </summary>
    public class PriceAnalyser
    {
        private static double MinPosition { get; } = 0.3;
        private static int EvaluationNextPos { get; } = 10;

        private ScorableNetwork scorableNetwork { get; }

        private List<IchimokuUnit> ichimoku;

        public PriceAnalyser(ScorableNetwork scorableNetwork)
        {
            this.scorableNetwork = scorableNetwork;
            this.scorableNetwork.takenPos = new Dictionary<DateTime,double>();
        }

        public void computeAnalyse(List<IchimokuUnit> ichimoku)
        {
            this.ichimoku = ichimoku;
            scorableNetwork.finalScore = 0;

           int index = 0;
            foreach (IchimokuUnit u in ichimoku)
            {
                //On skip 26 pour laisser le temps à la laging span d'avoir le nuage
                //added delay for current day
                if (index<26 || index> ichimoku.Count()-27)
                {
                    index++;
                    continue;
                }
         

                var vision = VisionAnalyses.generateVision(ichimoku, index).ToArray();
                var result = scorableNetwork.compute(vision);

                double risk = result[0];

                this.scorableNetwork.takenPos.Add(u.time, risk);

                if (risk > 0)
                {
                    this.takePostionAndScoring(ichimoku, index,risk);
                }
 



                index++;
            }
        }


        private void takePostionAndScoring(List<IchimokuUnit> candles,int positionIndex, double size)
        {
            if (positionIndex + EvaluationNextPos > candles.Count)
                return;

            var origin = candles[positionIndex];
            var destination = candles[positionIndex + EvaluationNextPos];

            double pluevalue = destination.priceClose - origin.priceClose;

            //double score = Math.Pow(pluevalue, 2);
            double score = pluevalue*size;
            scorableNetwork.finalScore += score;

        }
    }
}
