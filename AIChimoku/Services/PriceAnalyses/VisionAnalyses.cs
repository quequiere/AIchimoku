﻿using AIChimoku.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AIChimoku.Services.PriceAnalyses
{
    public class VisionAnalyses
    {
        public static IEnumerable<double> generateVision(List<IchimokuUnit> units, int current)
        {
            // On regarde les 10 dernieres positions des prix par rapports aux indicateurs
            for(int x = 0; x < 10;x++)
            {
                var prices = units[current+x];
                var candleRelativeProfile = getPointRelativePosition(prices.priceClose, prices);
                yield return candleRelativeProfile.ssa;
                yield return candleRelativeProfile.ssb;
                yield return candleRelativeProfile.kinjun;
                yield return candleRelativeProfile.tenkan;
            }
            


            // On regarde les obstables a franchir par la lagginSpan
            var laggingSpan = units[current - 26];
            var laggingRelativeProfile = getPointRelativePosition(laggingSpan.lagging, laggingSpan);

            yield return laggingRelativeProfile.ssa;
            yield return laggingRelativeProfile.ssb;
            yield return laggingRelativeProfile.kinjun;
            yield return laggingRelativeProfile.tenkan;
            yield return laggingRelativeProfile.price;



            for (int x = 0; x < 26; x++)
            {
                // On regarde la forme du futur nuage sur les prochaines périodes
                var cloud = units[current + x];
                var cloudRelativeProfile = getPointRelativePosition(units[current].priceClose, cloud);

                yield return cloudRelativeProfile.ssa;
                yield return cloudRelativeProfile.ssb;
            }

        }

        private static (double ssa,double ssb ,double kinjun,double tenkan, double price) getPointRelativePosition(double reference, IchimokuUnit unit)
        {
            var distSSA = reference / unit.SSA;
            var distSSB = reference / unit.SSB;
            var distKinjun = reference / unit.kinjun;
            var distTenkan = reference / unit.tenkan;
            var distPrice = reference / unit.priceClose;

            return (distSSA, distSSB, distKinjun, distTenkan, distPrice);
        }
       
    }
}
