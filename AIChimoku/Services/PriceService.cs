﻿using AIChimoku.Models;
using AIChimoku.Providers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AIChimoku.Services
{
    public class PriceService
    {
        private readonly IPriceProvider iPriceProvider;

        public PriceService(IPriceProvider iPriceProvider)
        {
            this.iPriceProvider = iPriceProvider;
        }


        public List<Candle> getDayHistory()
        {
            var prices = iPriceProvider.GetDailyCandles();
            return prices.response
                .Select(c => new Candle() {
                    C = c.c,
                    H = c.h,
                    L = c.l,
                    O = c.o,
                    Time = c.tm
                })
                .ToList();
        }

       

    }
}
